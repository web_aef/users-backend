import { IsNotEmpty, MinLength, IsInt } from 'class-validator';
export class CreateProductDto {
  @IsNotEmpty()
  @MinLength(5)
  name: string;

  @IsNotEmpty()
  @IsInt()
  price: number;
}
